package kimholan.bsn;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class BurgerServiceNummersTest {

    private static final int EXPECTED_VALID_BSN_COUNT = 90_000_000;
    private static final int BYTES_PER_INT = 4;
    private static final String FILENAME_BSN_VALID = "src/test/resources/burgerservicenummers-valid.bin";
    private static final List<Integer> CHECKSUM_ZERO_BSN = List.of(10000008, 100000009);
    private static final List<Integer> VALID_BSN = List.of(27566249, 13211250);
    private static final List<Integer> INVALID_BSN = List.of(0, 9999991, 1000000002);
    private static final String DEVANAGARI_DIGIT = "०१२३४५६७८९";
    private static IntBuffer validBsns;

    @BeforeAll
    public static void beforeAll() throws IOException {
        try (var valid = new RandomAccessFile(FILENAME_BSN_VALID, "rw")) {
            var buffer = ByteBuffer.allocateDirect(EXPECTED_VALID_BSN_COUNT * BYTES_PER_INT /* bytes */);
            var readBytes = valid.getChannel().read(buffer);
            assertEquals(EXPECTED_VALID_BSN_COUNT * BYTES_PER_INT, readBytes);
            validBsns = buffer.flip().asIntBuffer().asReadOnlyBuffer();
        }
    }

    @AfterAll
    public static void afterAll() {
        validBsns = null;
    }

    @BeforeEach
    public void beforeEach() {
        validBsns.rewind();
    }

    @Test
    public void invalidBsn() {
        INVALID_BSN.stream()
                .map(BurgerServiceNummers::isElfProef)
                .forEach(Assertions::assertFalse);

        INVALID_BSN.stream()
                .map(String::valueOf)
                .map(BurgerServiceNummers::isElfProef)
                .forEach(Assertions::assertFalse);
    }

    @Test
    public void validBsnDevIsInvalidAsDevanagari() {
        for (var bsn : VALID_BSN) {
            var devanagari = toDevanagari(bsn);
            var integerFromDevanagari = Integer.parseInt(devanagari);
            assertEquals(integerFromDevanagari, bsn);
            assertFalse(BurgerServiceNummers.isElfProef(devanagari));
            assertTrue(BurgerServiceNummers.isElfProef(integerFromDevanagari));
        }
    }


    @Test
    public void validBsn() {
        VALID_BSN.stream()
                .map(BurgerServiceNummers::isElfProef)
                .forEach(Assertions::assertTrue);

        VALID_BSN.stream()
                .map(String::valueOf)
                .map(BurgerServiceNummers::isElfProef)
                .forEach(Assertions::assertTrue);
    }

    @Test
    public void checksumZero() {
        CHECKSUM_ZERO_BSN.stream()
                .map(BurgerServiceNummers::isElfProef)
                .forEach(Assertions::assertTrue);

        CHECKSUM_ZERO_BSN.stream()
                .map(String::valueOf)
                .map(BurgerServiceNummers::isElfProef)
                .forEach(Assertions::assertTrue);
    }

    @Test
    public void isElfProefInt() {
        while (validBsns.hasRemaining()) {
            var candidateBsn = validBsns.get();
            assertTrue(BurgerServiceNummers.isElfProef(candidateBsn), () -> "Should be elf proef:" + candidateBsn);
        }
    }

    @Test
    public void isElfProefString() {
        while (validBsns.hasRemaining()) {
            var candidateBsn = String.valueOf(validBsns.get());
            assertTrue(BurgerServiceNummers.isElfProef(candidateBsn), () -> "Should be elf proef:" + candidateBsn);
        }
    }


    private String toDevanagari(Integer integer) {
        var asciiDigit = String.valueOf(integer);
        var builder = new StringBuilder();
        for (char digit : asciiDigit.toCharArray()) {
            builder.append(DEVANAGARI_DIGIT.charAt(Character.digit(digit, 10)));
        }
        return builder.toString();
    }
}
