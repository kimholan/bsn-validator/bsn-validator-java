package kimholan.bsn;

/**
 * <pre>
 * 11-proef
 * Het burgerservicenummer bestaat uit 9 cijfers en voldoet aan een variant op de elfproef.
 *
 * De variant is in het laatste cijfer, dat in plaats van met 1, met -1 wordt vermenigvuldigd.
 *
 * Dit verschil is er opzettelijk ingebracht zodat een abusievelijk ingevoerd bankrekeningnummer als foutief wordt aangemerkt:
 * dit gaat natuurlijk niet op als het nummer eindigt op een 0, in welk geval een nummer zowel een geldig bankrekeningnummer als BSN kan zijn.
 * Voor het burgerservicenummer geldt in ieder geval dat het wel degelijk op een 0 mag eindigen).
 *
 * Als het burgerservicenummer wordt voorgesteld door ABCDEFGHI, dan moet
 *
 * (9 × A) + (8 × B) + (7 × C) + (6 × D) + (5 × E) + (4 × F) + (3 × G) + (2 × H) + (-1 × I)
 *
 * een veelvoud van 11 zijn. Er kunnen met deze combinatie bijna 91 miljoen nummers gecreëerd worden. Geldige voorbeelden zijn: 111222333 en 123456782.
 * </pre>
 */
public enum BurgerServiceNummersAlternative {
;

    public static boolean isElfProef(String bsn) {
        var chars = bsn.toCharArray();

        if (chars.length == 9) {
            var sum = 9 * (chars[0] - 48)
                    + 8 * (chars[1] - 48)
                    + 7 * (chars[2] - 48)
                    + 6 * (chars[3] - 48)
                    + 5 * (chars[4] - 48)
                    + 4 * (chars[5] - 48)
                    + 3 * (chars[6] - 48)
                    + 2 * (chars[7] - 48)
                    - 1 * (chars[8] - 48);

            var multiplier = sum / 11;
            return multiplier * 11 == sum;
        } else {
            var sum = 8 * (chars[0] - 48)
                    + 7 * (chars[1] - 48)
                    + 6 * (chars[2] - 48)
                    + 5 * (chars[3] - 48)
                    + 4 * (chars[4] - 48)
                    + 3 * (chars[5] - 48)
                    + 2 * (chars[6] - 48)
                    - 1 * (chars[7] - 48);

            var multiplier = sum / 11;
            return multiplier * 11 == sum;
        }

    }

    /**
     * Validate BSN according to http://nl.wikipedia.org/wiki/Burgerservicenummer
     */
    public static boolean isValidBSN(int candidate) {
        if (candidate <= 9999999 || candidate > 999999999) {
            return false;
        }
        var sum = -1 * candidate % 10;

        for (var multiplier = 2; candidate > 0; multiplier++) {
            var val = (candidate /= 10) % 10;
            sum += multiplier * val;
        }

        return sum != 0 && sum % 11 == 0;
    }

    /**
     * Validate BSN according to http://nl.wikipedia.org/wiki/Burgerservicenummer
     */
    public static boolean isValidBSNFixedElfProefOnly(int candidate) {
        var sum = -1 * candidate % 10;

        for (var multiplier = 2; candidate > 0; multiplier++) {
            var val = (candidate /= 10) % 10;
            sum += multiplier * val;
        }

        return sum % 11 == 0;
    }


}
