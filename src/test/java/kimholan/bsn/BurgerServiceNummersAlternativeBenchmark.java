package kimholan.bsn;

import org.junit.jupiter.api.Test;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Warmup;

import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.openjdk.jmh.annotations.Mode.AverageTime;

/**
 * Measure how long it takes to find all BSNs in the range (inclusive) 10_000_000  to (exclusive) 1_000_000_000.
 */
public class BurgerServiceNummersAlternativeBenchmark {

    private static final int EXPECTED_VALID_BSN_COUNT = 90_000_000;

    @Test
    public void benchmark() throws Exception {
        String[] argv = {};
        org.openjdk.jmh.Main.main(argv);
    }

    @Benchmark
    @BenchmarkMode(AverageTime)
    @Fork(warmups = 1, value = 1)
    @Warmup(iterations = 1)
    @Measurement(iterations = 1)
    @OutputTimeUnit(MILLISECONDS)
    public void BurgerServiceNummers_isElfProef() {
        var n = 0;
        for (var i = 10_000_000; i < 1000_000_000; i++) {
            if (BurgerServiceNummers.isElfProef(i)) {
                n++;
            }
        }
        assertEquals(EXPECTED_VALID_BSN_COUNT, n);
    }

    @Benchmark
    @BenchmarkMode(AverageTime)
    @Fork(warmups = 1, value = 1)
    @Warmup(iterations = 1)
    @Measurement(iterations = 1)
    @OutputTimeUnit(MILLISECONDS)
    public void BurgerServiceNummersAlternative_isElfProef() {
        var n = 0;
        for (var i = 10_000_000; i < 1000_000_000; i++) {
            if (BurgerServiceNummersAlternative.isElfProef(String.valueOf(i))) {
                n++;
            }
        }
        assertEquals(EXPECTED_VALID_BSN_COUNT, n);
    }

    @Benchmark
    @BenchmarkMode(AverageTime)
    @Fork(warmups = 1, value = 1)
    @Warmup(iterations = 1)
    @Measurement(iterations = 1)
    @OutputTimeUnit(MILLISECONDS)
    public void BurgerServiceNummersAlternative_isValidBSN() {
        var n = 0;
        for (var i = 10_000_000; i < 1000_000_000; i++) {
            if (BurgerServiceNummersAlternative.isValidBSN(i)) {
                n++;
            }
        }
        assertEquals(89_999_998, n);
    }

    @Benchmark
    @BenchmarkMode(AverageTime)
    @Fork(warmups = 1, value = 1)
    @Warmup(iterations = 1)
    @Measurement(iterations = 1)
    @OutputTimeUnit(MILLISECONDS)
    public void BurgerServiceNummersAlternative_isValidBSNFixedElfProefOnly() {
        var n = 0;
        for (var i = 10_000_000; i < 1000_000_000; i++) {
            if (BurgerServiceNummersAlternative.isValidBSNFixedElfProefOnly(i)) {
                n++;
            }
        }
        assertEquals(EXPECTED_VALID_BSN_COUNT, n);
    }

}
