package kimholan.bsn;

import org.apache.commons.lang3.ArrayUtils;
import org.junit.jupiter.api.Test;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.TearDown;
import org.openjdk.jmh.annotations.Warmup;
import org.openjdk.jmh.infra.Blackhole;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;

import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import static org.openjdk.jmh.annotations.Level.Iteration;
import static org.openjdk.jmh.annotations.Level.Trial;
import static org.openjdk.jmh.annotations.Mode.Throughput;


public class BurgerServiceNummersBenchmark {

    private static final int EXPECTED_VALID_BSN_COUNT = 90_000_000;
    private static final int BYTES_PER_INT = 4;
    private static final String FILENAME_BSN_VALID = "src/test/resources/burgerservicenummers-valid.bin";

    /**
     * Generate binary dump of all valid bsns.
     */
    public static void main(String[] s) throws IOException {
        try (var valid = new RandomAccessFile(FILENAME_BSN_VALID, "rw")) {
            var buffer = ByteBuffer.allocateDirect(EXPECTED_VALID_BSN_COUNT * BYTES_PER_INT /* bytes */);
            valid.setLength(0);

            var n = 0;
            for (var i = 10_000_000; i < 1000_000_000; i++) {
                if (BurgerServiceNummers.isElfProef(i)) {
                    n++;
                    buffer.putInt(i);
                }
            }
            buffer.rewind();
            var bytesWritten = valid.getChannel().write(buffer);

            assertEquals(EXPECTED_VALID_BSN_COUNT * BYTES_PER_INT, bytesWritten);
            assertEquals(EXPECTED_VALID_BSN_COUNT, n);
        }

        try (var valid = new RandomAccessFile(FILENAME_BSN_VALID, "r")) {
            var buffer = ByteBuffer.allocateDirect(EXPECTED_VALID_BSN_COUNT * BYTES_PER_INT);
            var bytesRead = valid.getChannel().read(buffer);
            buffer.flip();

            assertEquals(EXPECTED_VALID_BSN_COUNT * BYTES_PER_INT, bytesRead);

            for (var n = 0; n < EXPECTED_VALID_BSN_COUNT; n++) {
                var bsn = buffer.getInt();
                assertTrue(BurgerServiceNummers.isElfProef(bsn), n + ":" + bsn + " is not valid");
            }
        }
    }

    @Test
    public void benchmark() throws Exception {
        new Runner(new OptionsBuilder()
                .include(this.getClass().getName() + ".*")
                // Set the following options as needed
                .shouldFailOnError(true)
                .shouldDoGC(true)
                .build()).run();
    }

    @Benchmark
    @BenchmarkMode(Throughput)
    @Fork(warmups = 1, value = 1)
    @Warmup(iterations = 10, time = 1)
    @Measurement(iterations = 3, time = 1)
    @OutputTimeUnit(MILLISECONDS)
    public void elfProef(Preloaded preloaded, Blackhole blackhole) {
        int candidateBsn = preloaded.intBuffer.get();

        if (BurgerServiceNummers.isElfProef(candidateBsn)) {
            blackhole.consume(candidateBsn);
        } else {
            fail();
        }
    }

    @Benchmark
    @BenchmarkMode(Throughput)
    @Fork(warmups = 1, value = 1)
    @Warmup(iterations = 10, time = 1)
    @Measurement(iterations = 3, time = 1)
    @OutputTimeUnit(MILLISECONDS)
    public void elfProefString(Preloaded preloaded, Blackhole blackhole) {
        var candidateBsn = preloaded.intBuffer.get();
        var candidateBsnString = String.valueOf(candidateBsn);

        if (BurgerServiceNummers.isElfProef(candidateBsnString)) {
            blackhole.consume(candidateBsnString);
        } else {
            fail(candidateBsnString);
        }
    }


    @State(Scope.Thread)
    public static class Preloaded {

        public int[] intBufferArray;
        public int readBytes;
        public IntBuffer intBuffer;

        @Setup(Trial)
        public void trial() throws IOException {
            try (var valid = new RandomAccessFile(FILENAME_BSN_VALID, "rw")) {
                var buffer = ByteBuffer.allocateDirect(EXPECTED_VALID_BSN_COUNT * BYTES_PER_INT /* bytes */);
                readBytes = valid.getChannel().read(buffer);
                assertEquals(EXPECTED_VALID_BSN_COUNT * BYTES_PER_INT, readBytes);
                intBufferArray = new int[EXPECTED_VALID_BSN_COUNT];
                buffer.flip().asIntBuffer().get(intBufferArray, 0, intBufferArray.length);
            }
        }

        @Setup(Iteration)
        public void iterator() {
            ArrayUtils.shuffle(intBufferArray);
            intBuffer = IntBuffer.wrap(intBufferArray);
        }

        @TearDown(Iteration)
        public void tearDown() {
            if (intBuffer.position() > EXPECTED_VALID_BSN_COUNT) {
                throw new Error();
            }
        }
    }
}
