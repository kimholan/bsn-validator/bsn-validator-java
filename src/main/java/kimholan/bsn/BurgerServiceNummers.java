package kimholan.bsn;

/**
 * <pre>
 * 11-proef
 * Het burgerservicenummer bestaat uit 9 cijfers en voldoet aan een variant op de elfproef.
 *
 * De variant is in het laatste cijfer, dat in plaats van met 1, met -1 wordt vermenigvuldigd.
 *
 * Dit verschil is er opzettelijk ingebracht zodat een abusievelijk ingevoerd bankrekeningnummer als foutief wordt aangemerkt:
 * dit gaat natuurlijk niet op als het nummer eindigt op een 0, in welk geval een nummer zowel een geldig bankrekeningnummer als BSN kan zijn.
 * Voor het burgerservicenummer geldt in ieder geval dat het wel degelijk op een 0 mag eindigen).
 *
 * Als het burgerservicenummer wordt voorgesteld door ABCDEFGHI, dan moet
 *
 * (9 × A) + (8 × B) + (7 × C) + (6 × D) + (5 × E) + (4 × F) + (3 × G) + (2 × H) + (-1 × I)
 *
 * een veelvoud van 11 zijn. Er kunnen met deze combinatie bijna 91 miljoen nummers gecreëerd worden. Geldige voorbeelden zijn: 111222333 en 123456782.
 * </pre>
 */
public enum BurgerServiceNummers {
    ;

    private static final int ASCII_DIGIT_OFFSET = 48;

    public static boolean isElfProef(String candidateBsn) {
        char[] chars = candidateBsn.toCharArray();
        if (!isAllAsciiDigits(chars)) return false;

        if (chars.length == 9) {
            return isElfProef(
                    chars[0] - ASCII_DIGIT_OFFSET,
                    chars[1] - ASCII_DIGIT_OFFSET,
                    chars[2] - ASCII_DIGIT_OFFSET,
                    chars[3] - ASCII_DIGIT_OFFSET,
                    chars[4] - ASCII_DIGIT_OFFSET,
                    chars[5] - ASCII_DIGIT_OFFSET,
                    chars[6] - ASCII_DIGIT_OFFSET,
                    chars[7] - ASCII_DIGIT_OFFSET,
                    chars[8] - ASCII_DIGIT_OFFSET);
        } else if (chars.length == 8) {
            return isElfProef(
                    0,
                    chars[0] - ASCII_DIGIT_OFFSET,
                    chars[1] - ASCII_DIGIT_OFFSET,
                    chars[2] - ASCII_DIGIT_OFFSET,
                    chars[3] - ASCII_DIGIT_OFFSET,
                    chars[4] - ASCII_DIGIT_OFFSET,
                    chars[5] - ASCII_DIGIT_OFFSET,
                    chars[6] - ASCII_DIGIT_OFFSET,
                    chars[7] - ASCII_DIGIT_OFFSET);
        } else {
            return false;
        }
    }

    private static boolean isAllAsciiDigits(char[] chars) {
        for (var c : chars) {
            if (c < '0' || c > '9') return false;
        }
        return true;
    }

    private static boolean isElfProef(int a_div_100_000_000,
                                      int b_div_10_000_000,
                                      int c_div_1_000_000,
                                      int d_div_100_000,
                                      int e_div_10_000,
                                      int f_div_1_000,
                                      int g_div_100,
                                      int h_div_10,
                                      int i_div_1) {
        var checksum = 9 * a_div_100_000_000
                + 8 * b_div_10_000_000
                + 7 * c_div_1_000_000
                + 6 * d_div_100_000
                + 5 * e_div_10_000
                + 4 * f_div_1_000
                + 3 * g_div_100
                + 2 * h_div_10
                - i_div_1;

        var multiplier = checksum / 11;
        var value = multiplier * 11;
        return value == checksum &&
                (a_div_100_000_000 > 0) ||
                (b_div_10_000_000 > 0);
    }


    public static boolean isElfProef(int candidateBsn) {
        if (candidateBsn < 10_000_000 || candidateBsn > 1_000_000_000) return false;

        var val = candidateBsn;

        var a = val / 100_000_000;
        val -= a * 100_000_000;

        var b = val / 10_000_000;
        val -= b * 10_000_000;

        var c = val / 1_000_000;
        val -= c * 1_000_000;

        var d = val / 100_000;
        val -= d * 100_000;

        var e = val / 10_000;
        val -= e * 10_000;

        var f = val / 1_000;
        val -= f * 1_000;

        var g = val / 100;
        val -= g * 100;

        var h = val / 10;
        val -= h * 10;

        var i = val;

        var sum = 9 * a
                + 8 * b
                + 7 * c
                + 6 * d
                + 5 * e
                + 4 * f
                + 3 * g
                + 2 * h
                - i;

        var multiplier = sum / 11;
        return multiplier * 11 == sum;
    }

}
