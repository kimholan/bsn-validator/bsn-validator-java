package kimholan.bsn;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class BurgerServiceNummersAlternativeTest {

    private final List<Integer> SUM_ZERO_BSN = List.of(10000008, 100000009);

    @Test
    public void isElfProef() {
        SUM_ZERO_BSN.forEach(it -> assertTrue(BurgerServiceNummersAlternative.isElfProef(String.valueOf(it))));
    }

    @Test
    public void isValidBSNFixedElfProefOnly() {
        SUM_ZERO_BSN.forEach(it -> assertTrue(BurgerServiceNummersAlternative.isValidBSNFixedElfProefOnly(it)));
    }

    @Test
    public void isValidBSN() {
        SUM_ZERO_BSN.forEach(it -> assertThrows(AssertionError.class, () -> assertTrue(BurgerServiceNummersAlternative.isValidBSN(it))));
    }


}
